from os import listdir
from os.path import isfile, join
from . import Log

class OneWire:
    def __init__(self):
        self.rootdir="/sys/bus/w1/"
        self.log=Log()
        self.log.level=Log.Error
    def ListDevices(self):
        res=[]
        for f in listdir(join(self.rootdir,"devices")):
            if isfile(join(mypath, f)):
                res.append(f)
        return res
    def Read(self,device):
        if(device==""):
            return None
        if(device==None):
            return None
        filename=join('/sys/bus/w1/devices/',device,'w1_slave')
        self.log.D("Reading device node <%s>",filename)
        file = open(filename)
        res = file.read()
        file.close()
        self.log.D("Read RAW Value: <%s>",res)
        return res
    def ReadTemperature(self,device):
        raw=self.Read(device)
        if(raw==None):
            return None
        stringvalue = raw.split("\n")[1].split(" ")[9]
        return float(stringvalue[2:]) / 1000
