import json
from . import Log
import requests
import socketio

class Client:
    def __init__(self,server="127.0.0.1:5000"):
        self.server=server
        self.base="/api/v2/"
        self.log=Log()
        self.log.level=Log.Debug
    def RegisterSensor(self,id):
        a=Node()
        url="".join(["http://",self.server,self.base,"datastreams/",id])
        self.log.D("connect to Stream <%s>",url)
        a.Connect(url)
        return a
    def ConnectStream(self,id):
        a=Stream()
        url="".join(["ws://",self.server,self.base,"realtime"])
        self.log.D("connect to Stream <%s>",url)
        a.Connect(url,id)
        return a

class Node:
    def __init__(self):
        self.url=None
        self.timeout=0.1
    def Connect(self,url):
        self.url=url
    def Send(self,value):
        requests.post(self.url,json=json.dumps(value), timeout=self.timeout)
    def Get(self):
        requests.get(self.url, timeout=self.timeout)

class Stream:
    def __init__(self):
        self.io=socketio.Client()
        self.key="unkown"
        self.updateCallbackMethod=None
    def Connect(self,addr,key):
        self.io.connect(addr)
        self.key=key
    def Send(self,value):
        self.io.emit("update",{self.key:value})
    def OnUpdate(self,method):
        self.io.on(self.key,method)
    def Disconnect(self):
        self.io.close()
    def Wait(self):
        self.io.wait()
